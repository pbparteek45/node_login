const mongoose = require('mongoose');

const eventDetail = mongoose.Schema({
    eventname: { type: String,required: true},
    event_date: {type: String},
    event_type: { type: String, required: true},
    event_subcategory: { type: String,required: true},
    userid:{type:mongoose.Schema.ObjectId,ref:'user'}
},
    {
        timestamps: true
    });

module.exports = mongoose.model('event', eventDetail)
