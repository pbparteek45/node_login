const Note = require('../models/note.model.js');
const eventDetail = require('../models/event.model.js');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var config = require('./config');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');

module.exports = {
    register: (req, res) => {
        var hashpassword = bcrypt.hashSync(req.body.password);
        const note = new Note({
            firstname: req.body.firstname,
            email: req.body.email,
            lastname: req.body.lastname,
            password: hashpassword
        });
        var date = new Date();
        var time = date.getTime();

        Note.findOne({ email: req.body.email }, function (err, user) {
            if (err) {
                throw err
            }
            if (user) {
                res.send({ message: 'email already exists' })
            }
            else {
                note.save().then(data => {
                    res.status(200).send({ data: data, message: time, success: true, message: 'register successfully & mail has sent to you' });
                })
                let smtpTransport = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                    user: 'codetest04@gmail.com',
                    pass: 'qiauykqeghwpikil'
                    }
                    });
                    let mailOptions = {
                    from: 'codetest04@gmail.com', // sender address
                    to: req.body.email, // list of receivers
                    subject: "welcome", // Subject line
                    // text: "welcome"+req.body.firstname+"to our site", // plain text body
                    html: '<p>hello '+req.body.firstname+' Thanks for registration with us </p>'
                    };
    
                    smtpTransport.sendMail(mailOptions, function (err, info) {
                        if (err) {
                        res.json({ success: false, message: 'mail was not send ' })
                        } else {
                        res.json({ success: true, message: 'mail was sent successfully' })
                        }
                    })
            }
           
               
        })
    },

    login: (req, res) => {
        if (req)
            Note.findOne({
                email: req.body.email
            }, function (err, user) {
                if (err)
                    throw err;
                if (!user) {
                    res.json({ success: false, message: 'authentication failed. User not found' })
                } else if (user) {
                    bcrypt.compare(req.body.password, user.password, function (err, bycryptRes) {
                        if (bycryptRes) {
                            var token = jwt.sign({ user: user._id }, config.secret, { expiresIn: '1d' });
                            
                            res.status(200).send({ "status": "successfully log in", "token": token, "success": true });
                        } else {
                            res.send({ message: 'password did not match', "success": false })
                        }
                    })
                }
            })
    },

    add_events: (req, res) => {
        var token = req.headers.authorization;
        if(token!==null){
            jwt.verify(token,config.secret,function(err,decode){
                if(decode){
                    console.log('just a test');
                    const event = new eventDetail({
                        userid: decode.user,
                        eventname: req.body.eventname,
                        event_date: req.body.event_date,
                        event_type: req.body.event_type,
                        event_subcategory: req.body.event_subcategory
                    });
                eventDetail.findOne({ eventname: req.body.eventname,userid: decode.user }, function (err, data) {
                    if (err) {
                        throw err
                    }
                    if (data) {
                        res.send({ message: 'event already exists' })
                    }
                    else {
                        event.save().then(data => {
                            res.status(200).send({ data: data,success: true, message: 'eventDetail Created Succesfully' });
                        })
                    }
                })
                    
                }
                else{
                    res.status(401).json({success:false, message:"token expires"})
                }
            })
        }

        else{
            res.status(401).json({success:false,message:"token not found"})
        }
    },

    show_events: (req,res)=>{
        var token = req.headers.authorization;
        if (token) {
            jwt.verify(token, config.secret, function (err, decode) {
                if (decode) {
                    eventDetail.find({ userid: decode.user }, function (err, user) {
                        if (user) {
                            console.log(user);
                            res.status(200).json({ success: true, message: 'fetch data successfully', data: user })
                        } else {
                            res.status(401).send({ success: false, message: 'detail not found' })
                        }
                    })
                } else {
                    res.status(401).json({ message: 'user not found' })
                }
            })
        } else {
            res.status(401).json({ success: false, message: 'token was not found' })
        }
    },

    user_profile:(req,res)=>{
        var token = req.headers.authorization;
        if(token){
            jwt.verify(token,config.secret,function(err,decode){ 
                 if(decode){ 
                /*     eventDetail.findOne({ userid:decode.user }).populate('userid',{firstname:1,lastname:1}).
                        exec((err,data)=>{
                            if (data) {
                                console.log(data);
                                res.status(200).json({ success: true, message: 'fetch data successfully', data: data })
                            } else {
                                res.status(401).send({ success: false, message: 'detail not found' })
                            }
                            
                    })  */                  
                  /*   eventDetail.aggregate([
                        {
                            $match: {userid:mongoose.Types.ObjectId(decode.user)}
                        },    
                        {
                            $lookup : {
                                from: "users", 
                                localField: "userid", 
                                foreignField: "_id", 
                                as: "user_detail" 
                            }
                        }
                    ]).exec(( err,us_de)=>{
                        if(us_de){
                           res.status(200).json({ success:true,message:"data fetch successfully",data:us_de}) 
                        }
                        else{
                                 console.log('prblm');
                           res.status(401).send({ success:false, message:'not found'})
                        }
                    }) */
                }
            })
        }
    },

    socialSignup:(req,res)=>{
        if(req){
            Note.findOne({ email: req.body.email },function(err,user){
                if(user){
                    res.status(409).json({ success:false ,message:'user already exists'})
                }
                else {
                    var str = req.body.name
                    var name = str.split(" ");
                    const note = new Note({
                        firstname:name[0],
                        email:req.body.email,
                        lastname:name[1],
                        password:req.body.password == undefined ? '': req.body.password
                    });
                    note.save().then(data=>{
                        res.status(200).json({ success:true,message:'user registered successfully'})
                    })
                }
            })
        }
    }
    
}


