
module.exports = (app) => {
    const notes = require('../controllers/user.controller.js')
    // create new Note
    app.post('/register', notes.register);
    
    //get user signin
    app.post('/signin',notes.login);

    app.post('/add_events',notes.add_events);

    app.get('/show_events',notes.show_events);

    app.get('/user_profile',notes.user_profile);

    app.post('/social_signup',notes.socialSignup);

}

