const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express ();
app.use(bodyParser.urlencoded({extended:true}))
app.use(cors('*'))

app.use(bodyParser.json())

const dbConfig = require('./database.config.js');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url,{
    useNewUrlParser:true
}).then(()=>{
    console.log('successfully connected to the database')
}).catch(err => {
    console.log('Could not connect to the database.Exiting Now...',err);
    process.exit()
});


require('./app/routes/note.routes.js')(app);


app.listen(3001, () => {
    console.log("Server is listening on port 3001")
})